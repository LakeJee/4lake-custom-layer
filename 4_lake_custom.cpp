#if !defined(FCODER_DEFAULT_BINDINGS_CPP)
#define FCODER_DEFAULT_BINDINGS_CPP

#include "4coder_default_include.cpp"
#pragma warning(disable : 4189)

// NOTE(allen): Users can declare their own managed IDs here.

#if !defined(META_PASS)
#include "generated/managed_id_metadata.cpp"
#endif

static void
Lake_SetCustomHooks(Application_Links *app) {
    // NOTE(lake): no-op right now want to see if it will compile first
    
} 

void
custom_layer_init(Application_Links *app){
    Thread_Context *tctx = get_thread_context(app);
    
    // NOTE(allen): setup for default framework
    default_framework_init(app);
    
    // NOTE(allen): default hooks and command maps
    set_all_default_hooks(app);
    mapping_init(tctx, &framework_mapping);
    String_ID global_map_id = vars_save_string_lit("keys_global");
    String_ID file_map_id = vars_save_string_lit("keys_file");
    String_ID code_map_id = vars_save_string_lit("keys_code");
#if OS_MAC
    setup_mac_mapping(&framework_mapping, global_map_id, file_map_id, code_map_id);
#else
    setup_default_mapping(&framework_mapping, global_map_id, file_map_id, code_map_id);
#endif
    setup_essential_mapping(&framework_mapping, global_map_id, file_map_id, code_map_id);

    /* Custom mapping start */
    MappingScope();
    SelectMapping( &framework_mapping );
    SelectMap( global_map_id );

    // NOTE(lake): for whatever reason this binding is not working...
    Bind( toggle_fullscreen, KeyCode_V, KeyCode_Control, KeyCode_Alt );
    /* Custom mapping end */
}

#endif //FCODER_DEFAULT_BINDINGS

// BOTTOM